package RuleChecker.Rules;

import RuleChecker.RuleResult;
import domain.Person;
import org.junit.Test;

import java.util.Date;

import static org.junit.Assert.*;

public class PersonDateOfBirthRuleTest {

    @Test
    public void testCheckRule() throws Exception {
        PersonDateOfBirthRule rule = new PersonDateOfBirthRule();
        Person p = new Person();
        Date date = new Date(1995,11,2);
        p.setDateOfBirth(date);
        assertEquals(rule.checkRule(p).getResult(),RuleResult.Ok);
    }

    @Test
    public void testDateTooFar() throws Exception {
        PersonDateOfBirthRule rule = new PersonDateOfBirthRule();
        Person p = new Person();
        Date date = new Date(1756,11,2);
        p.setDateOfBirth(date);
        rule.checkRule(p);
        assertFalse(rule.getIsDateValid());
    }

    @Test
    public void testDateTooEarly() throws Exception {
        PersonDateOfBirthRule rule = new PersonDateOfBirthRule();
        Person p = new Person();
        Date date = new Date(2013,11,2);
        p.setDateOfBirth(date);
        rule.checkRule(p);
        assertFalse(rule.getIsDateValid());
    }

    @Test
    public void testTodayDate() throws Exception {
        PersonDateOfBirthRule rule = new PersonDateOfBirthRule();
        Person p = new Person();
        Date date = new Date();
        p.setDateOfBirth(date);
        rule.checkRule(p);
        assertFalse(rule.getIsDateValid());
    }
}
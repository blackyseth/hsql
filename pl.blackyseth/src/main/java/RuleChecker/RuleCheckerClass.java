package RuleChecker;

import java.util.ArrayList;
import java.util.List;

public class RuleCheckerClass<TEntity> {
    private List<ICanCheckRule<TEntity>> rules = new ArrayList<>();

    public List<ICanCheckRule<TEntity>> getRules() {
        return rules;
    }

    public void setRules(List<ICanCheckRule<TEntity>> rules) {
        this.rules = rules;
    }

    public List<CheckResult> check(TEntity entity){
        List<CheckResult> result = new ArrayList<>();
        for (ICanCheckRule<TEntity> rule: rules){
            result.add(rule.checkRule(entity));
        }
        return result;
    }
}

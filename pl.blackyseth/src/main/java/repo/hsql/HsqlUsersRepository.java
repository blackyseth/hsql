package repo.hsql;

import domain.Entity;

import domain.UserRoles;
import repo.PagingInfo;
import repo.UnitOfWork.IUnitOfWork;
import repo.UnitOfWork.UnitOfWorkRepository;
import domain.User;
import repo.UserRepository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class HsqlUsersRepository implements UserRepository,UnitOfWorkRepository{

    private String sql;
    private PreparedStatement statement;
    private Connection connection;
    private IUnitOfWork unitOfWork;

    public HsqlUsersRepository(Connection connection, IUnitOfWork unitOfWork){
        this.unitOfWork = unitOfWork;
        this.connection = connection;
    }

    public void withLogin(String login) {

    }

    public void withLoginAndPassword(String login, String password) {

    }

    public void setupPermissions(User user) {
        UserRoles role = new UserRoles();
        user.setRoles(role);
    }

    public void withId(int id) {

    }

    public void allOnPage(PagingInfo page) {

    }

    public void add(User user) {
        unitOfWork.markAsNew(user,this);
    }

    public void delete(User user) {
        unitOfWork.markAsDeleted(user, this);
    }

    public void modify(User user) {
        unitOfWork.markAsChanged(user, this);
    }

    public int count() {
        return getAll().size();
    }

    @Override
    public List<User> getAll() {
        return new ArrayList<>();
    }


    @Override
    public void persistAdd(Entity entity){

        try {
            sql = "INSERT INTO t_sys_enums VALUES (?, ?, ?, ?);";
            statement.setInt(1,entity.getId());
            statement.setInt(2,entity.getId());
            statement.setInt(3,entity.getId());
            statement.setInt(4,entity.getId());
            statement = connection.prepareStatement(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void persistDelete(Entity entity) {

    }

    @Override
    public void persistUpdate(Entity entity) {

    }
}

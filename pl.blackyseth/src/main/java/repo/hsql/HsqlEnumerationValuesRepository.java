package repo.hsql;
import repo.EnumerationValueRepository;
import repo.PagingInfo;
import repo.UnitOfWork.IUnitOfWork;
import repo.UnitOfWork.UnitOfWorkRepository;

import domain.Entity;
import domain.EnumerationValue;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

public class HsqlEnumerationValuesRepository implements EnumerationValueRepository, UnitOfWorkRepository{

    private Connection connection;
    private IUnitOfWork unitOfWork;

    public HsqlEnumerationValuesRepository(Connection connection, IUnitOfWork unitOfWork) {
        this.unitOfWork = unitOfWork;
        this.connection = connection;
    }

    public void withName(String name) {

    }

    public void withIntKey(int key, String name) {

    }

    public void withStringKey(String key, String name) {

    }

    public void withId(int id) {

    }

    public void allOnPage(PagingInfo page) {

    }

    public void add(EnumerationValue user) {
        unitOfWork.markAsNew(user,this);
    }

    public void delete(EnumerationValue user) {
        unitOfWork.markAsDeleted(user, this);
    }

    public void modify(EnumerationValue user) {
        unitOfWork.markAsChanged(user, this);
    }

    public int count() {
        return getAll().size();
    }

    @Override
    public List<EnumerationValue> getAll() {
        return new ArrayList<>();
    }


    @Override
    public void persistAdd(Entity entity) {

    }

    @Override
    public void persistDelete(Entity entity) {

    }

    @Override
    public void persistUpdate(Entity entity) {

    }
}

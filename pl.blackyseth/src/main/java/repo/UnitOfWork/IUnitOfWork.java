package repo.UnitOfWork;

import domain.Entity;

public interface IUnitOfWork {
    void commit();
    void rollback();
    void markAsNew(Entity entity,UnitOfWorkRepository unitOfWorkRepository);
    void markAsDeleted(Entity entity,UnitOfWorkRepository unitOfWorkRepository);
    void markAsChanged(Entity entity, UnitOfWorkRepository unitOfWorkRepository);
}

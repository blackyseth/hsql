package repo.UnitOfWork;


import domain.Entity;
import domain.EntityState;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;

public class UnitOfWork implements  IUnitOfWork{

    private Connection connection;
    private Map<Entity,UnitOfWorkRepository> entities;

    public UnitOfWork(Connection connection){
        this.connection = connection;
        try {
            connection.setAutoCommit(false);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void commit() {
        try {
            for (Entity entity: entities.keySet()){
                switch (entity.getState()){
                    case Modified: {entities.get(entity).persistUpdate(entity);break;}
                    case Deleted: {entities.get(entity).persistDelete(entity);break;}
                    case New: {entities.get(entity).persistDelete(entity);break;}
                    case UnChanged: break;
                    default: break;
                }
            }
            connection.commit();
            entities.clear();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void rollback() {
        entities.clear();
    }

    @Override
    public void markAsNew(Entity entity, UnitOfWorkRepository unitOfWorkRepository) {
        entity.setState(EntityState.New);
        entities.put(entity,unitOfWorkRepository);
    }

    @Override
    public void markAsDeleted(Entity entity, UnitOfWorkRepository unitOfWorkRepository) {
        entity.setState(EntityState.Deleted);
        entities.put(entity,unitOfWorkRepository);
    }

    @Override
    public void markAsChanged(Entity entity, UnitOfWorkRepository unitOfWorkRepository) {
        entity.setState(EntityState.Modified);
        entities.put(entity,unitOfWorkRepository);
    }
}

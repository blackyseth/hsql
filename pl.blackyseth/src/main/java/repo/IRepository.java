package repo;

import domain.Entity;

import java.util.List;

public interface IRepository<TEntity extends Entity>{
    void withId(int id);
    void allOnPage(PagingInfo page);
    void add(TEntity entity);
    void delete(TEntity entity);
    void modify(TEntity entity);
    int count();
    List<TEntity> getAll();
}

package repo;

import repo.UnitOfWork.IUnitOfWork;
import repo.hsql.HsqlEnumerationValuesRepository;
import repo.hsql.HsqlUsersRepository;

import java.sql.Connection;

public class RepositoryCatalog implements IRepositoryCatalog{

    private Connection connection;
    private IUnitOfWork unitOfWork;

    public RepositoryCatalog(Connection connection, IUnitOfWork unitOfWork) {
        this.connection = connection;
        this.unitOfWork = unitOfWork;
    }

    public EnumerationValueRepository enumerations() {
        return new HsqlEnumerationValuesRepository(connection,unitOfWork);
    }

    public UserRepository users() {
        return new HsqlUsersRepository(connection,unitOfWork);
    }

    @Override
    public void commit() {
        unitOfWork.commit();
    }
}

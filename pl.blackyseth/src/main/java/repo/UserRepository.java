package repo;

import domain.User;

public interface UserRepository extends IRepository<User>{
    void withLogin(String login);
    void withLoginAndPassword(String login, String password);
    void setupPermissions(User user);
}

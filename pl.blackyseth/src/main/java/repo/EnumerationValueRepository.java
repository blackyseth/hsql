package repo;

import domain.EnumerationValue;

public interface EnumerationValueRepository extends IRepository<EnumerationValue>{
    void withName(String name);
    void withIntKey(int key, String name);
    void withStringKey(String key, String name);
}

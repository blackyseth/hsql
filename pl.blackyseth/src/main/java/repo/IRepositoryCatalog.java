package repo;

import domain.EnumerationValue;
import domain.User;

public interface IRepositoryCatalog {
    EnumerationValueRepository enumerations();
    UserRepository users();
    void commit();
}

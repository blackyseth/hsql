package domain;

public class EnumerationValue extends Entity{
    private int intKey;
    private int stringKey;
    private int value;
    private String enumerationName;

    public int getIntKey() {
        return intKey;
    }

    public void setIntKey(int intKey) {
        this.intKey = intKey;
    }

    public int getStringKey() {
        return stringKey;
    }

    public void setStringKey(int stringKey) {
        this.stringKey = stringKey;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getEnumerationName() {
        return enumerationName;
    }

    public void setEnumerationName(String enumerationName) {
        this.enumerationName = enumerationName;
    }
}

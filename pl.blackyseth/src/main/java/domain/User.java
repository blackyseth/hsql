package domain;

import java.util.ArrayList;
import java.util.List;

public class User extends Entity{
    private String login;
    private String password;
    private List<UserRoles> roles;

    public User(){
        super();
        roles = new ArrayList<UserRoles>();
    }

    public List<UserRoles> getRoles() {
        return roles;
    }

    public void setRoles(UserRoles role) {
        roles.add(role);
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}

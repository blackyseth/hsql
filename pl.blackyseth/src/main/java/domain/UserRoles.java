package domain;

import java.util.List;

public class UserRoles extends Entity{
    private String name;
    private List<RolesPermissions> roles;
    private List<User> users;

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public List<RolesPermissions> getRoles() {
        return roles;
    }

    public void setRoles(List<RolesPermissions> roles) {
        this.roles = roles;
    }



}

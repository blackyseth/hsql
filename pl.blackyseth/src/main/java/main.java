import repo.IRepositoryCatalog;
import repo.RepositoryCatalog;
import repo.UnitOfWork.IUnitOfWork;
import repo.UnitOfWork.UnitOfWork;
import domain.User;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;

public class main {
    public static void main(String[] args) {
        String url="jdbc:hsqldb:mem:HSQLDB";
        User someuser = new User();
        someuser.setLogin("login");
        someuser.setPassword("pass");
        try{
            Class.forName("org.hsqldb.jdbc.JDBCDriver");
            Connection connection = DriverManager.getConnection(url);

            IUnitOfWork unitOfWork = new UnitOfWork(connection);
            IRepositoryCatalog repositoryCatalog = new RepositoryCatalog(connection,unitOfWork);

            repositoryCatalog.users().setupPermissions(someuser);
            List<User> userList = repositoryCatalog.users().getAll();
            System.out.println(repositoryCatalog.users().count());

            for(User userfromDB: userList){
                System.out.println(userfromDB.getLogin());
            }


        } catch (SQLException|ClassNotFoundException e2) {
            e2.printStackTrace();
        }
    }
}
